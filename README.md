# b_tagging_LIP

This repository is a collection of notebooks used to study b-tagging algorithms on the context of Heavy Ion data. This readme serves as an introduction to the files in here.

The original files can be found in https://gitlab.cern.ch/mguth/aml-workshop19-tutorials/-/tree/master/btagging-ml_tutorial, which is a tutorial for developing a ML classifier for b-tagging, still in the context of proton-proton collisions.

The files in this repo are edited versions of these, more specifically:

## Data Files
JZ1_5_even.h5 and JZ1_5_odd.h5 are two halves of data generated from  simulation of HI Collisions. These are the starting samples.

## Sample Preparation
The files ending in HI_Sample_Prep are jupyter notebook concerned with receiving the sample as input and preparing it for training. They process the data and generate training, validation and testing set. They are commented in order to understand what they do. For the purpose of this readme, just take note of the following - there are two sample_prep notebooks, because of an idea to compare three different methods of preparing the sets:

### Original_method
This notebook generates a set of training, validation and test sets as the original one did, as well as a second set of these slightly tweaked. The key difference is that it completely separates JZ1_even from JZ1_odd for the training set and the validation, testing sets, respectively. As you will see in the notebooks, the data for the training set goes through a downsampling process that the testing/validation will not, which really reduces the amount of data in the training set. Thus, the original method had way bigger validation sets than training sets, which was identified as a potential problem later in the training stage. (Usually in ML, the training set is considerably bigger than the validation set. A usual proportion which will be used later is 70% training/15% validation/15% testing).

The slightly tweaked method is also applied in this notebook, as, to solve this proportion problem, it uses the same process and just eliminates a big cut of the validation data, at the end, in order to fit the proportion. 

### My_method
The other notebook (my_method_HI_sample_prep) applies the third method to generate sets. It previews the big cut downsampling will have on the training data, and takes that into account before the downsampling. It joins JZ1_even and JZ1_odd in one single set, and separates it into a pre-calculated proportion (about 97% goes through to downsampling and becomes the training set, and rest wil be split into training and test set), in order to end with the proportion of 70/15/15.

The datasets generated from these methods were stored in the folders "three_datasets_run1" and in "three_datasets". Those labeled "1_all" correspond to the original method, "1_proportional" to the tweaked original method and "2" to my method.

## Training Notebook
The notebook "HI_train_DL1_3flav.ipynb" takes in the sets produced and trains a NN, producing ROC curves with its results.

## ROC Curve Notebook
The notebook "ROC_Curve_Analysis.ipynb" was produced after noticing the need to compare ROC curves from different runs of the training notebook (in different conditions - for example, to compare the results produced from the datasets produced from the three methods). It takes in data saved by the training notebook in the folder "ROC_curve_data" and compares it. 


## Final notes
This repo was made in a very informal manner and the commenting and documenting is still an ongoing process. If certains parts are not clear enough, feel free to contact me (Vicente) with any question :D

The sample preparation sets can be very expensive on memory. You need at least 16GB in order to run, and even then you can encounter problems like a usual "kernel died" error. In those cases, remember you can run only the cells you want to run in the notebook, in the order you want. For example, if you have already got the training set, but it crashes before you get validation and test sets, you can run the notebook again but only the part that generates the latter sets. Hopefully this won't be needed!